from typing import Optional, List

from tests.utils import UnorderedListEq


def pytest_assertrepr_compare(
    config: "Config", op: str, left: object, right: object
) -> Optional[List[str]]:
    if isinstance(left, UnorderedListEq) and isinstance(right, list) and op == "==":
        if hasattr(left, "item_not_found"):
            if hasattr(left, "in_expected_list"):
                contained_in_label, not_contained_in_label = "actual", "expected"
                contained_in_value, not_contained_in_value = left.actual, left.in_expected_list
            else:
                contained_in_label, not_contained_in_label = "expected", "actual"
                contained_in_value, not_contained_in_value = left.in_expected_list, left.actual

            reason = [
                f"Item {left.item_not_found}, contained in {contained_in_label} list:",
                f"  {contained_in_value}",
                f"is not contained in {not_contained_in_label} list:",
                f"  {not_contained_in_value}",
            ]
        elif hasattr(left, "expected_len"):
            reason = [
                f"Actual and expected lists contain the same set of items, but the lists differ in length, because of some duplicate items",
                f"  len(actual) == {len(left.actual)}, len(expected) == {left.expected_len}",
            ]
        else:
            return None

        return [
            "Comparing lists in unordered fashion:",
            *reason,
        ]
